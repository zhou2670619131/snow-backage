//package com.snow.config;
//
//import com.theokanning.openai.OpenAiService;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//
//import java.time.Duration;
//
///**
// * @description: openai配置
// * @author: Ddalao
// * @create: 2023-12-13 23:01
// **/
//public class OpenAiConfiguration {
//
//    //open ai 密钥配置
//    @Value("${open.ai.key}")
//    private String openAiKey;
//    //请求超时配置
//    @Value("${open.ai.request.timeout}")
//    private long timeout;
//    //初始化
//
//    @Bean
//    public OpenAiService openAiService(){
//        return new OpenAiService(openAiKey, Duration.ofSeconds(timeout));
//    }
//
//}