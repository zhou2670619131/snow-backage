package com.snow.mapper;

import com.snow.domain.TrItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Ddala
* @description 针对表【tr_item】的数据库操作Mapper
* @createDate 2023-12-09 16:34:52
* @Entity com.snow.domain.TrItem
*/
@Mapper
public interface TrItemMapper extends BaseMapper<TrItem> {

}




