package com.snow.mapper;

import com.snow.domain.TrScenic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Ddala
* @description 针对表【tr_scenic】的数据库操作Mapper
* @createDate 2023-12-09 16:26:14
* @Entity generator.domain.TrScenic
*/
@Mapper
public interface TrScenicMapper extends BaseMapper<TrScenic> {

}




