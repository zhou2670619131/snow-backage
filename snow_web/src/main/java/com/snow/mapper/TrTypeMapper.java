package com.snow.mapper;

import com.snow.domain.TrType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Ddala
* @description 针对表【tr_type】的数据库操作Mapper
* @createDate 2023-12-09 16:53:22
* @Entity com.snow.domain.TrType
*/
@Mapper
public interface TrTypeMapper extends BaseMapper<TrType> {

}




