package com.snow.controller;

import com.snow.domain.TrItem;
import com.snow.service.TrItemService;
import com.snow.util.Result;
import com.snow.util.ResultGenerator;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 旅游项目控制器
 * @author: Ddalao
 * @create: 2023-12-09 16:33
 **/
@RestController
@RequestMapping("item")
public class ItemController {
    @Resource
    TrItemService trItemService;

    @GetMapping("getItem")
    @CrossOrigin
    public Result getItem(@RequestParam(name = "scenicId", required = false) String scenicId,
                          @RequestParam(name = "type", required = false) String type){
        List<TrItem> trItems= trItemService.getItem(scenicId,type);
        return ResultGenerator.genSuccessResult(trItems);
    }
}