package com.snow.controller;


import com.alibaba.fastjson.JSONArray;
import com.snow.domain.TrScenic;
import com.snow.service.TrScenicService;
import com.snow.util.Result;
import com.snow.util.ResultGenerator;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 景点控制器
 * @author: Ddalao
 * @create: 2023-12-09 16:25
 **/
@RestController
@RequestMapping("scenic")
@CrossOrigin(origins = "*",maxAge = 3600)
public class ScenicController {
    @Resource
    TrScenicService trScenicService;

    @GetMapping("getScenicList")
    public Result getScenicList(){
        List<TrScenic> list = trScenicService.getScenicList();
        return ResultGenerator.genSuccessResult(list);
    }


    @GetMapping("getScenic")
    public Result getScenic(@RequestParam(name = "scenicId", required = false) String scenicId){
        TrScenic scenic = trScenicService.getById(scenicId);
        return ResultGenerator.genSuccessResult(scenic);
    }

        @PostMapping("addScenic")
    public Result addScenic(@RequestBody TrScenic trScenic){
        Integer integer = trScenicService.addScenic(trScenic);
        return integer==0?ResultGenerator.genFailResult("添加失败"):ResultGenerator.genSuccessResult();

    }
}