package com.snow.controller;

import com.snow.domain.TrType;
import com.snow.service.TrTypeService;
import com.snow.util.Result;
import com.snow.util.ResultGenerator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 项目类型控制器
 * @author: Ddalao
 * @create: 2023-12-09 16:53
 **/
@RestController
@RequestMapping("itemType")
public class ItemTypeController {
    @Resource
    TrTypeService trTypeService;

    @GetMapping("getType")
    public Result getType(){
        List<TrType> list = trTypeService.list();
        return ResultGenerator.genSuccessResult(list);
    }
}