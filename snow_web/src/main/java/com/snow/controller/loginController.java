package com.snow.controller;

import com.snow.domain.Bo.UserBo;
import com.snow.util.Result;
import com.snow.util.ResultGenerator;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("user")
public class loginController {
    @PostMapping("login")
    public Result Login(@RequestBody UserBo userBo){

        Result result = ResultGenerator.genSuccessResult(userBo);
//        result.setCode(0);
        return  result;


    }
}
