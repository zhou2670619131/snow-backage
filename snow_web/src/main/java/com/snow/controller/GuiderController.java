package com.snow.controller;

import com.plexpt.chatgpt.ChatGPT;
import com.plexpt.chatgpt.ChatGPTStream;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.listener.SseStreamListener;
import com.plexpt.chatgpt.util.Proxys;
import com.snow.config.SseEmitterUTF8;
import com.snow.domain.TrItem;
import com.snow.util.Result;
import com.snow.util.ResultGenerator;
import org.apache.dubbo.common.utils.Log;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import java.io.IOException;
import java.net.Proxy;
import java.util.Arrays;
import java.util.List;

/**
 * @description: ai导游
 * @author: Ddalao
 * @create: 2023-12-14 16:30
 **/
@RestController
@RequestMapping("guider")
public class GuiderController {

    @GetMapping("getGuiderScenic")
    @CrossOrigin
    public SseEmitter getItem(@RequestParam("scenic") String scenic){
        Proxy proxy = Proxys.http("60.204.204.123", 7890);

        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .apiKey("sk-p4xGZpKxT4z4BssgA3B8Ff370c18471eA51451F5149bA447")
                .timeout(900)
                .apiHost("https://oneapi.xty.app/") //反向代理地址
                .build()
                .init();


        Message system = Message.ofSystem("你是一个非常专业的导游，你只会回答关于旅游方面的问题，不能回答除了旅游类型以外的任何问题！旅游以外的问题你只会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”，如果我问你的是一个不存在的景点你也会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”");
        Message message = Message.of("给我全方位，从各个角度分别介绍一下这几个景点：" + scenic);

        return getSseEmitter(chatGPTStream, system, message);
    }

    @GetMapping("getFreeGuider")
    @CrossOrigin
    public SseEmitter getFreeGuider(@RequestParam("text") String text){
        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .apiKey("sk-p4xGZpKxT4z4BssgA3B8Ff370c18471eA51451F5149bA447")
                .timeout(900)
                .apiHost("https://oneapi.xty.app/") //反向代理地址
                .build()
                .init();


        Message system = Message.ofSystem("你是一个非常专业的导游，你只会回答关于旅游方面的问题，旅游以外的问题你只会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”");
        Message message = Message.of(text);

        return getSseEmitter(chatGPTStream, system, message);
    }





    @GetMapping("/sse")
    @CrossOrigin
    public SseEmitter sseEmitter(String prompt) {
        //国内需要代理 国外不需要
//        Proxy proxy = Proxys.http("localhost", 7890);

        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .apiKey("sk-p4xGZpKxT4z4BssgA3B8Ff370c18471eA51451F5149bA447")
                .timeout(900)
                .apiHost("https://oneapi.xty.app/") //反向代理地址
                .build()
                .init();



        Message system = Message.ofSystem("你是一个非常专业的导游，你只会回答关于旅游方面的问题，旅游以外的问题你只会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”");
        Message message = Message.of("给我介绍冰雪大世界" );
        return getSseEmitter(chatGPTStream, system, message);
    }

    private SseEmitter getSseEmitter(ChatGPTStream chatGPTStream, Message system, Message message) {
        SseEmitterUTF8 sseEmitter = new SseEmitterUTF8(-1L);

        SseStreamListener listener = new SseStreamListener(sseEmitter);


        listener.setOnComplate(msg -> {
            //回答完成，可以做一些事情
            System.out.println("完成");
//            // 关闭SSE连接
//            sseEmitter.complete();
        });
        chatGPTStream.streamChatCompletion(Arrays.asList(message,system), listener);
        return sseEmitter;
    }
}