package com.snow.service;

import com.snow.domain.TrType;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
* @author Ddala
* @description 针对表【tr_type】的数据库操作Service
* @createDate 2023-12-09 16:53:22
*/

public interface TrTypeService extends IService<TrType> {

}
