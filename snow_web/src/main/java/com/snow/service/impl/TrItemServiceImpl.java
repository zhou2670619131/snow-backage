package com.snow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snow.domain.TrItem;
import com.snow.service.TrItemService;
import com.snow.mapper.TrItemMapper;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
* @author Ddala
* @description 针对表【tr_item】的数据库操作Service实现
* @createDate 2023-12-09 16:34:52
*/
@Service
public class TrItemServiceImpl extends ServiceImpl<TrItemMapper, TrItem>
    implements TrItemService{

    @Override
    public List<TrItem> getItem(String scenicId,String type) {
        LambdaQueryWrapper<TrItem> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(StringUtils.hasText(scenicId),TrItem::getCsenicId,scenicId)
                .eq(StringUtils.hasText(type),TrItem::getType,type)
                .select(TrItem::getCsenicId,TrItem::getId,TrItem::getImages,TrItem::getItemName);
        return list(lambdaQueryWrapper);
    }
}




