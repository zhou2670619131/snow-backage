package com.snow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snow.domain.TrType;
import com.snow.service.TrTypeService;
import com.snow.mapper.TrTypeMapper;
import org.springframework.stereotype.Service;

/**
* @author Ddala
* @description 针对表【tr_type】的数据库操作Service实现
* @createDate 2023-12-09 16:53:22
*/
@Service
public class TrTypeServiceImpl extends ServiceImpl<TrTypeMapper, TrType>
    implements TrTypeService{

}




