package com.snow.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.snow.domain.TrScenic;
import com.snow.service.TrScenicService;
import com.snow.mapper.TrScenicMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Ddala
* @description 针对表【tr_scenic】的数据库操作Service实现
* @createDate 2023-12-09 16:26:14
*/
@Service
public class TrScenicServiceImpl extends ServiceImpl<TrScenicMapper, TrScenic>
    implements TrScenicService {

    @Override
    public List<TrScenic> getScenicList() {
        LambdaQueryWrapper<TrScenic> queryWrapper = new LambdaQueryWrapper<>();
        ObjectMapper objectMapper = new ObjectMapper();
//        queryWrapper.select(TrScenic::getScenicName,TrScenic::getId,TrScenic::getImages);
        List<TrScenic> list = list(queryWrapper);
        for (TrScenic trScenic : list) {
            Object images = trScenic.getImages();
            String cleanedString = images.toString().replaceAll("\\\\\"", "");
            try {
                trScenic.setImages( objectMapper.readValue((String) images, new TypeReference<List<String>>(){}));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    public Integer addScenic(TrScenic trScenic) {
//        Object images = trScenic.getImages();
//        String s = images.toString();
//        String[] split = s.split(",");
//        ObjectMapper objectMapper = new ObjectMapper();
//        try {
//            String s1 = objectMapper.writeValueAsString(s);
//            trScenic.setImages(s1);
//
//
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
        int insert = baseMapper.insert(trScenic);
        return insert;
    }

    @Override
    public Integer delete(String id) {
        int i = baseMapper.deleteById(id);
        return i;
    }
}




