package com.snow.service;

import com.snow.domain.TrScenic;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Ddala
* @description 针对表【tr_scenic】的数据库操作Service
* @createDate 2023-12-09 16:26:14
*/

public interface TrScenicService extends IService<TrScenic> {

    List<TrScenic> getScenicList();
    public Integer addScenic(TrScenic trScenic);
    public Integer delete(String id);
}
