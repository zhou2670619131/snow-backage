package com.snow.service;

import com.snow.domain.TrItem;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Ddala
* @description 针对表【tr_item】的数据库操作Service
* @createDate 2023-12-09 16:34:52
*/

public interface TrItemService extends IService<TrItem> {

    List<TrItem> getItem(String scenicId,String type);
}
