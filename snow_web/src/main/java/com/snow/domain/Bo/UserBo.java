package com.snow.domain.Bo;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserBo implements Serializable {
    public String userAccount;
    public String userPassword;
}
