package com.snow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName tr_type
 */
@TableName(value ="tr_type")
@Data
public class TrType implements Serializable {
    /**
     * 项目类型名称
     */
    @TableId
    private String type;

    /**
     * 类型封面图片
     */
    private String image;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}