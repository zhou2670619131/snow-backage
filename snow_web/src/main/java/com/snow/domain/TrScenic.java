package com.snow.domain;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;

/**
 * 
 * @TableName tr_scenic
 */
@TableName(value ="tr_scenic")
@Data
public class TrScenic implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 景点名称
     */
    private String scenicName;

    /**
     * 营业时间，例如 10:00-22:00
     */
    private String openHours;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 景点图片，多个图片以','隔开
     */
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private Object images;

    /**
     * 地址
     */
    private String address;

    /**
     * 经度
     */
    private Double x;

    /**
     * 维度
     */
    private Double y;

    /**
     * 逻辑删除
     */
    private String delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}