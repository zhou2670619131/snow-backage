package com.snow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName tr_item
 */
@TableName(value ="tr_item")
@Data
public class TrItem implements Serializable {
    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 景点id
     */
    private String csenicId;
    
    /**
     * 项目类型
     *
     * @param null
     * @return
     * @author Ddalao
     * @create 2023/12/9
     **/
    private String type;


    /**
     * 项目名称
     */
    private String itemName;

    /**
     * 项目图片，多个图片以','隔开
     */
    private String images;

    /**
     * 游玩简介
     */
    private String text;

    /**
     * 游玩贴士
     */
    private String help;

    /**
     * 逻辑删除
     */
    private String delFlag;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}