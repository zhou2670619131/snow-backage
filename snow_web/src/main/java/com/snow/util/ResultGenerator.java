package com.snow.util;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: result工具类
 * @author: Ddalao
 * @create: 2023-12-07 18:20
 **/
public class ResultGenerator {

    private static final String DEFAULT_SUCCESS_MESSAGE = "操作成功";
    private static final String GET_DATA_SUCCESS_MESSAGE = "获取数据成功";

    public static Result genSuccessResult() {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE);
    }

    public static Result genSuccessResult(String message) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(message);
    }

    public static Result genSuccessResult(Object data) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(DEFAULT_SUCCESS_MESSAGE)
                .setData(data);
    }

    public static Result genSuccessResult(String message,Object data) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(message)
                .setData(data);
    }

    public static Result genSuccessResult(long count,Object dataList) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(GET_DATA_SUCCESS_MESSAGE)
                .setCount(count)
                .setData(dataList);
    }

    public static Result genSuccessResult(String message, Page page) {
        return new Result()
                .setCode(ResultCode.SUCCESS)
                .setMessage(message)
                .setCount(page.getTotal())
                .setData(page.getRecords());
    }

    public static Result genFailResult(String message) {
        return new Result()
                .setCode(ResultCode.FAIL)
                .setMessage(message);
    }
}

