package com.snow.util;

import com.alibaba.fastjson.JSON;

import java.io.Serializable;

/**
 * @description: 统一返回
 * @author: Ddalao
 * @create: 2023-12-09 19:53
 **/
public class Result implements Serializable {
    private int code;
    private String message;
    private long count;
    private Object data;

    public Result setCode(ResultCode resultCode) {
        this.code = resultCode.code;
        return this;
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public long getCount() {
        return count;
    }

    public Result setCount(long count) {
        this.count = count;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Result setData(Object data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}