package com.snow;

import com.plexpt.chatgpt.ChatGPT;
import com.plexpt.chatgpt.ChatGPTStream;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.listener.ConsoleStreamListener;
import com.plexpt.chatgpt.util.Proxys;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.Proxy;
import java.util.Arrays;

/**
 * @description:
 * @author: Ddalao
 * @create: 2023-12-13 22:39
 **/
@SpringBootTest
public class testapp {

    @Test
    void Test(){
        //国内需要代理
        //国内需要代理 国外不需要
        Proxy proxy = Proxys.http("60.204.204.123", 7890);

        ChatGPT chatGPT = ChatGPT.builder()
                .apiKey("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")
                .proxy(proxy)
                .timeout(900)
                .apiHost("https://api.openai.com/") //反向代理地址
                .build()
                .init();

        Message system = Message.ofSystem("你是一个非常专业的导游，你只会回答关于旅游方面的问题，旅游以外的问题你只会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”");
        Message message = Message.of("给我推荐几个哈尔滨景点");

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .model(ChatCompletion.Model.GPT_3_5_TURBO.getName())
                .messages(Arrays.asList(system, message))
                .maxTokens(3000)
                .temperature(0.9)
                .build();
        ChatCompletionResponse response = chatGPT.chatCompletion(chatCompletion);
        Message res = response.getChoices().get(0).getMessage();
        System.out.println(res);

    }



    @Test
    public void test() {
        //国内需要代理 国外不需要
        Proxy proxy = Proxys.http("127.0.0.1", 1080);

        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .apiKey("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")
                .proxy(proxy)
                .timeout(900)
                .apiHost("https://api.openai.com/") //反向代理地址
                .build()
                .init();


        Message system = Message.ofSystem("你是一个非常专业的导游，你只会回答关于旅游方面的问题，旅游以外的问题你只会回答：“这个问题我还不会，让我们聊聊旅游景点吧！”");
        Message message = Message.of("给我全方位，从各个角度分别介绍一下这几个景点：");


        ConsoleStreamListener listener = new ConsoleStreamListener();

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .messages(Arrays.asList(message))
                .build();
        chatGPTStream.streamChatCompletion(chatCompletion, listener);
    }
}