package com.snow.mapper;

import com.snow.domain.TrBlog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Ddala
* @description 针对表【tr_blog】的数据库操作Mapper
* @createDate 2023-12-07 18:16:57
* @Entity generator.domain.TrBlog
*/
@Mapper
public interface TrBlogMapper extends BaseMapper<TrBlog> {

}




