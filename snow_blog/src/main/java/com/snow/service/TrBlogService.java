package com.snow.service;

import com.snow.domain.TrBlog;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

/**
* @author Ddala
* @description 针对表【tr_blog】的数据库操作Service
* @createDate 2023-12-07 18:16:57
*/
@Service
public interface TrBlogService extends IService<TrBlog> {

}
