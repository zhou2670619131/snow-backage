package com.snow.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.snow.domain.TrBlog;
import com.snow.service.TrBlogService;
import com.snow.mapper.TrBlogMapper;
import org.springframework.stereotype.Service;

/**
* @author Ddala
* @description 针对表【tr_blog】的数据库操作Service实现
* @createDate 2023-12-07 18:16:57
*/
@Service
public class TrBlogServiceImpl extends ServiceImpl<TrBlogMapper, TrBlog>
    implements TrBlogService{

}




