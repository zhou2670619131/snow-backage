package com.snow.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * 
 * @TableName tr_blog
 */
@TableName(value ="tr_blog")
@Data
public class TrBlog implements Serializable {

    /**
     * 主键
     */
    @TableId
    private String id;

    /**
     * 景点id

     */
    private String sceneryId;

    /**
     * 标题
     */
    private String title;

    /**
     * blog正文
     */
    private String text;

    /**
     * blog的照片，最多9张，多张以","隔开
     */
    private String images;

    /**
     * blog的文字描述
     */
    private String content;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 逻辑删除
     */
    private String delFlag;

    @TableField(exist = false)
    private List<String> image;
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}