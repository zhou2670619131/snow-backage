package com.snow.controller;

import com.snow.domain.TrBlog;
import com.snow.service.TrBlogService;
import com.snow.util.R;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

/**
 * @description: blog模块控制器
 * @author: Ddalao
 * @create: 2023-12-07 18:17
 **/
@RestController
@RequestMapping("blog")
public class BlogController {


    @Resource
    TrBlogService trBlogService;

    @GetMapping("getBlog")
    @CrossOrigin
    public R getBlog(){
        List<TrBlog> list = trBlogService.list();
        // 使用 Lambda 表达式为每个 TrBlog 对象处理
        list.forEach(trBlog -> trBlog.setImage(Arrays.asList(trBlog.getImages().split(","))));
        return R.ok().put("data",list);
    }

    @GetMapping("getBlogById")
    @CrossOrigin
    public R getBlogById(@RequestParam("id") String id){
        TrBlog trBlog = trBlogService.getById(id);
        trBlog.setImage(Arrays.asList(trBlog.getImages().split(",")));
        return R.ok().put("data",trBlog);
    }
}